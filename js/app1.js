function cargarRazas() {
    const selectRaza = document.getElementById("raza");
    selectRaza.innerHTML = '<option>Cargando...</option>';

    axios.get('https://dog.ceo/api/breeds/list')
        .then(response => {
            const data = response.data;
            selectRaza.innerHTML = ''; // Limpiamos el contenido previo del select

            data.message.forEach(raza => {
                const option = document.createElement('option');
                option.text = raza;
                option.value = raza;
                selectRaza.add(option);
            });
        })
        .catch(error => console.error('Error al obtener la lista de razas:', error));
}

function mostrarImagen() {
    const selectRaza = document.getElementById("raza");
    const razaSeleccionada = selectRaza.value;
    const imagenPerro = document.getElementById("imagenPerro");

    if (!razaSeleccionada) {
        alert("Por favor selecciona una raza antes de mostrar la imagen.");
        return;
    }

    axios.get(`https://dog.ceo/api/breed/${razaSeleccionada}/images/random`)
        .then(response => {
            const imageUrl = response.data.message;
            imagenPerro.src = imageUrl;
            imagenPerro.alt = `Imagen de un ${razaSeleccionada}`;
        })
        .catch(error => console.error('Error al obtener la imagen:', error));
}
document.getElementById("btnListar").addEventListener('click', function () {
    cargarRazas();
});

document.getElementById("mostrarImg").addEventListener('click', function () {
    mostrarImagen();
});